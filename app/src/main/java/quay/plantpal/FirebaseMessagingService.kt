package quay.plantpal

import android.content.Context
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import quay.plantpal.Model.User

class FirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.from)
        Log.d(TAG, "Message Notification Body: " + remoteMessage.notification?.body)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        val sharedPreferences = getSharedPreferences("UserData", Context.MODE_PRIVATE)
        val userAsString = sharedPreferences.getString("user", null)
        if (userAsString != "" && userAsString != null) {
            val user = Gson().fromJson(userAsString, User::class.java)
            Global.user = user
            PlantPalAPI.registerDeviceToken(Global.user.uid, token) { success, error ->
                if (success) {
                    print("Success")
                    val editor = sharedPreferences.edit()
                    editor.putString("deviceToken", token)
                    editor.apply()
                }
                else {
                    print("Failed")
                }
            }
        }
    }

    companion object {
        private const val TAG = "MyTAG"
    }
}