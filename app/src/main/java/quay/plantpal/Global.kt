package quay.plantpal

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread
import quay.plantpal.Model.User

object Global {
    lateinit var user: User
    var prefsSet = false
    var appVersion = BuildConfig.VERSION_NAME

    fun logEvent(event: String, otherProperties: HashMap<String, String>? = null) {
        if (this::user.isInitialized) {
            val allProperties = otherProperties?.plus(hashMapOf("userId" to user.uid))
            Analytics.trackEvent("${BuildConfig.BUILD_TYPE}: $event", allProperties)
        }
        else {
            Analytics.trackEvent("${BuildConfig.BUILD_TYPE}: $event", otherProperties)
        }
    }

    fun displayErrorAlert(context: Context, title: String, message: String?) {
        runOnUiThread {
            logEvent("Displayed Error", hashMapOf("Title" to title, "Message" to (message ?: "N/A")))
            val builder = AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
            builder.setTitle(title)
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("Ok") { _, _ ->  }
            builder.create().show()
        }
    }
}