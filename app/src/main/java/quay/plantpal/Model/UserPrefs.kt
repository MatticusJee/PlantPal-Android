package quay.plantpal.Model

import org.json.JSONObject

class UserPrefs {

    var beginner: Boolean = false
    var intermediate: Boolean = false
    var expert: Boolean = false
    var cleanAir: Boolean = false
    var petFriendly: Boolean = false
    var colorful: Boolean = false
    var scented: Boolean = false
    var isSet = false

    constructor()

    constructor(json: JSONObject) {
        if (json.has("beginner")) {
            beginner = json.getBoolean("beginner")
            intermediate = json.getBoolean("intermediate")
            expert = json.getBoolean("expert")
            cleanAir = json.getBoolean("cleanAir")
            petFriendly = json.getBoolean("petFriendly")
            colorful = json.getBoolean("colorful")
            scented = json.getBoolean("scented")
            this.isSet = true
        }
    }
}
