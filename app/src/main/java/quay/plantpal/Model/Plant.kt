package quay.plantpal.Model

import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class Plant//"Wednesday, October 17, 2018, 7:22:27 PM"
(json: JSONObject) {

    enum class WateredStatus(val color: String) {
        Happy("#3E88DE"),
        NotBad("#F69C1E"),
        Sad("#B11915")
    }

    var plantDbId: Int? = null
    var pName: String? = null
    var userPlantId: Int? = null
    var pStatus: String? = null
    private var s3ImageKey: String? = null
    var imagePath: String? = null
    var uid: String? = null
    var pNickName: String? = null
    private var lastWatered: Date? = null
    private var hourlyWaterCycle: Int? = null
    var wateredStatus: WateredStatus? = null
    var temp: String? = null
    var difficulty: String? = null
    private var waterInfo: String? = null
    var lightInfo: String? = null
    var faq: String? = null
    var soilInfo: String? = null
    var fertilizerInfo: String? = null
    var cleansAir: Boolean? = null
    var isPetFriendly: Boolean? = null
    var isColorful: Boolean? = null
    var isScented: Boolean? = null

    init {
        plantDbId = json.getInt("plantId")
        pName = json.getString("pName")
        s3ImageKey = json.getString("S3ImageKey")
        imagePath = json.getString("imagePath")
        val hasStatus = json.has("pStatus")
        if(hasStatus) {
            pStatus = json.getString("pStatus")
            userPlantId = json.getInt("id")
            when (pStatus) {
                "MyPlants" -> {
                    uid = json.getString("userId")
                    pStatus = json.getString("pStatus")
                    pNickName = json.getString("pNickName")
                    //"Wednesday, October 17, 2018, 7:22:27 PM"
                    val dateFormat = SimpleDateFormat("EEEEE, MMMMM dd, yyyy, hh:mm:ss a", Locale.US)
                    dateFormat.timeZone = TimeZone.getTimeZone("UTC")
                    lastWatered = dateFormat.parse(json.getString("lastWatered"))
                    hourlyWaterCycle = json.getInt("hourlyWaterCycle")
                    val calendar = Calendar.getInstance()
                    calendar.time = lastWatered
                    calendar.add(Calendar.HOUR, hourlyWaterCycle!! / 3)
                    val endOfHappyWaterPeriod = calendar.time
                    calendar.time = lastWatered
                    calendar.add(Calendar.HOUR, (hourlyWaterCycle!! / 3) * 2)
                    val endOfNotBadWaterPeriod = calendar.time
                    val currentDate = Date()
                    wateredStatus = when {
                        currentDate <= endOfHappyWaterPeriod -> {
                            WateredStatus.Happy
                        }
                        currentDate <= endOfNotBadWaterPeriod -> {
                            WateredStatus.NotBad
                        }
                        else -> {
                            WateredStatus.Sad
                        }
                    }
                }
                "WishList" -> {
                    userPlantId = json.getInt("id")
                    uid = json.getString("userId")
                    pStatus = json.getString("pStatus")
                }
            }
        }
        else {
            temp = json.getString("TEMP")
            difficulty = json.getString("DIFFICULTY")
            waterInfo = json.getString("WATER")
            lightInfo = json.getString("LIGHT")
            if (json.getString("FAQ") != "null") {
                faq = json.getString("FAQ")
            }
            soilInfo = json.getString("SOIL")
            fertilizerInfo = json.getString("FERTILIZER")
            cleansAir = json.getBoolean("CLEAN_AIR")
            isPetFriendly = json.getBoolean("PET_FRIENDLY")
            isColorful = json.getBoolean("COLORFUL")
            isScented = json.getBoolean("SCENTED")
        }
    }
}