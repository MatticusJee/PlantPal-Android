package quay.plantpal.Model

import org.json.JSONObject

enum class LoginType(val type: String) {
    plantpal("Plant Pal"),
    google("Google"),
    facebook("Facebook"),
    snapchat("Snapchat")
}

class User {

    var uid: String
    var email: String
    var username: String
    var loginType: LoginType

    constructor(json: JSONObject) {
        uid = json.getInt("userId").toString()
        email = json.getString("email")
        username = json.getString("username")
        loginType = LoginType.valueOf(json.getString("accountType"))
    }

    constructor(uid: String, email: String, username: String, loginType: LoginType) {
        this.uid = uid
        this.email = email
        this.username = username
        this.loginType = loginType
    }
}
