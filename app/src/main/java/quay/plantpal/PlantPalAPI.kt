package quay.plantpal

import android.os.Build
import android.view.LayoutInflater
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import quay.plantpal.Activities.AllPlantsListViewRow
import quay.plantpal.Model.LoginType
import quay.plantpal.Model.Plant
import quay.plantpal.Model.User
import quay.plantpal.Model.UserPrefs
import java.io.IOException

object PlantPalAPI {

    private const val baseUrl = BuildConfig.BASE_URL

    private val JSON = "application/json; charset=utf-8".toMediaTypeOrNull()
    private val okHttpClient = OkHttpClient()

    // App Version

    fun setVersion(hasUserInfo: Boolean, callback: (Boolean, String?, String?) -> Unit) { //success, version, storeLink
        val path = "/users/appversion/"
        val url = "$baseUrl$path"
        var json = ""
        json = if (hasUserInfo) {
            "" +
                    "{\n" +
                    "    \"uid\": \"${Global.user.uid}\",\n" +
                    "    \"os\": \"Android\",\n" +
                    "    \"device\": \"${getDeviceName()}\"" +
                    "\n}"
        }
        else {
            "" +
                    "{\n" +
                    "    \"os\": \"Android\",\n" +
                    "    \"device\": \"${getDeviceName()}\"" +
                    "\n}"
        }
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }

            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string()).getJSONObject("data")
                    val version = jsonObject.getString("version")
                    callback(true, version, "")
                }
                else {
                    callback(false, null, null)
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    private fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else {
            capitalize(manufacturer) + " " + model
        }
    }

    private fun capitalize(s: String?): String {
        if (s == null || s.isEmpty()) {
            return ""
        }
        val first = s[0]
        return if (Character.isUpperCase(first)) {
            s
        } else {
            Character.toUpperCase(first) + s.substring(1)
        }
    }

    // Available Plants

    fun availablePlants(userId: Int, searchString: String, lastPlantLoaded: Int?, callback: (Boolean, ArrayList<AllPlantsListViewRow>?, Int?, UserPrefs?, String?) -> Unit) {
        val path = "/plants/db/results/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"userId\": $userId,\n" +
                "    \"searchString\": \"$searchString\",\n" +
                "    \"lastPlantKeyIndex\": $lastPlantLoaded" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null, null, null, "Something went wrong while trying to get available plants. Please try again")
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string())
                    val dataObject = jsonObject.getJSONObject("data")
                    val plantsJsonArray = dataObject.getJSONArray("Items")
                    var lastEvaluatedPlantIdKey: Int? = null
                    if (dataObject.has("LastEvaluatedKey")) {
                        lastEvaluatedPlantIdKey = dataObject.getJSONObject("LastEvaluatedKey").getInt("plantId")
                    }
                    val userPrefsJSON = jsonObject.getJSONObject("userPrefs")
                    val plantRows = arrayListOf<AllPlantsListViewRow>()
                    for (i in 0 until plantsJsonArray.length()) {
                        val plant = Plant(plantsJsonArray[i] as JSONObject)
                        plantRows.add(AllPlantsListViewRow(R.layout.all_plants_table_row, plant))
                    }
                    if (plantRows.isEmpty()) {
                        plantRows.add(AllPlantsListViewRow(R.layout.all_plants_empty_results_row))
                    }
                    val userPrefs = UserPrefs(userPrefsJSON)
                    callback(true, plantRows, lastEvaluatedPlantIdKey, userPrefs, null)
                }
                else {
                    callback(false, null, null, null, "Something went wrong while trying to get available plants. Please try again")
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Claim Plant

    fun claimPlant(id: Int?, uid: String, pid: Int, currentStatus: String?, newStatus: String, pName: String, pNickName: String?,
                   callback: (Boolean, String?) -> Unit) {
        val path = "/plants/setPlant/"
        val url = "$baseUrl$path"
        val json: String
        //AllPlants -> WishList
        if(id == null && currentStatus == null && pNickName == null) {
            json = "" +
                    "{\n" +
                    "    \"userId\": \"$uid\",\n" +
                    "    \"plantId\": $pid,\n" +
                    "    \"pName\": \"$pName\",\n" +
                    "    \"newStatus\": \"$newStatus\"" +
                    "\n}"
        }
        //AllPlants -> MyPlants
        else if(id == null && currentStatus == null) {
            json = "" +
                    "{\n" +
                    "    \"userId\": \"$uid\",\n" +
                    "    \"plantId\": $pid,\n" +
                    "    \"pName\": \"$pName\",\n" +
                    "    \"newStatus\": \"$newStatus\"\n," +
                    "    \"pNickName\": \"$pNickName\"" +
                    "\n}"
        }
        //WishList -> MyPlants
        else {
            json = "" +
                    "{\n" +
                    "    \"id\": $id,\n" +
                    "    \"userId\": \"$uid\",\n" +
                    "    \"plantId\": $pid,\n" +
                    "    \"currentStatus\": \"$currentStatus\",\n" +
                    "    \"newStatus\": \"$newStatus\",\n" +
                    "    \"pName\": \"$pName\",\n" +
                    "    \"pNickName\": \"$pNickName\"" +
                    "\n}"
        }
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }

            override fun onResponse(call: Call, response: Response) {
                val jsonObject = JSONObject(response.body!!.string())
                if(response.isSuccessful) {
                    callback(true, null)
                }
                else {
                    callback(false, jsonObject.getString("message"))
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Create User

    fun createUser(username: String, email: String, password: String, accountType: LoginType, callback: (Boolean, User?, String?) -> Unit) {
        val path = "/users/create/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"username\": \"$username\",\n" +
                "    \"email\": \"$email\",\n" +
                "    \"password\": \"$password\",\n" +
                "    \"accountType\": \"${accountType.name}\",\n" +
                "    \"os\": \"Android\"" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null, "Request Failed. Please Try again.")
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                val jsonObject = JSONObject(response.body!!.string())
                if(response.isSuccessful) {
                    val userId = jsonObject.getInt("userId")
                    val user = User(userId.toString(), email, username, accountType)
                    callback(true, user, null)
                }
                else {
                    val message = jsonObject.getString("message")
                    callback(false, null, message)
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Get Plant Info

    fun getPlantInfo(plantId: Int, callback: (Boolean, Plant?, String?) -> Unit) {
        val path = "/plants/plant/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"plantId\": $plantId" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string())
                    if(response.code == 200) {
                        val data = jsonObject.getJSONObject("data")
                        val plant = Plant(data)
                        callback(true, plant, null)
                    }
                    else {
                        val message = jsonObject.getString("message")
                        callback(true, null, message)
                    }
                }
                else {
                    val code = response.code.toString()
                    callback(false, null, "Error Code: $code")
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to code))
                }
            }
        })
    }

    // Get User Plants

    fun getUserPlants(uid: String, pStatus: String, callback: (Boolean, ArrayList<Plant>?, String?) -> Unit) {
        val path = "/users/getPlants/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"userId\": \"$uid\",\n" +
                "    \"pStatus\": \"$pStatus\"" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string()).getJSONArray("data")
                    val plants = arrayListOf<Plant>()
                    for(i in 0 until jsonObject.length()) {
                        plants.add(0, Plant(jsonObject[i] as JSONObject))
                    }
                    callback(true, plants, null)
                }
                else {
                    val message = JSONObject(response.body!!.string()).getString("message")
                    callback(false, null, message)
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Login

    fun login(usernameEmail: String, password: String, isUsingEmail: Boolean, accountType: LoginType, callback: (Boolean, User?, Boolean?, String?) -> Unit) {
        val path = "/users/login/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"email\": \"$usernameEmail\",\n" +
                "    \"password\": \"$password\",\n" +
                "    \"isUsingEmail\": $isUsingEmail,\n" +
                "    \"accountType\": \"${accountType.name}\"" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null, null, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string()).getJSONObject("data")
                    callback(true, User(jsonObject), UserPrefs(jsonObject).isSet, null)
                }
                else {
                    callback(false, null, null, "Error Code: ${response.code}")
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Register Device Token

    fun registerDeviceToken(userId: String, token: String, callback: (Boolean, String?) -> Unit) {
        val path = "/users/notifications/register/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"userId\": \"$userId\",\n" +
                "    \"os\": \"Android\",\n" +
                "    \"token\": \"$token\"" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string())
                    callback(true, null)
                }
                else {
                    callback(false, null)
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Remove Device Token

    fun removeDeviceToken(userId: String, token: String, callback: (Boolean, String?) -> Unit) {
        val path = "/users/notifications/removeToken/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"userId\": \"$userId\",\n" +
                "    \"os\": \"Android\",\n" +
                "    \"token\": \"$token\"" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).delete(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string())
                    callback(true, null)
                }
                else {
                    callback(false, null)
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Remove Plant

    fun removePlant(id: Int, uid: String, pStatus: String, callback: (Boolean, String?) -> Unit) {
        val path = "/users/plants/remove/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"id\": $id,\n" +
                "    \"userId\": \"$uid\",\n" +
                "    \"pStatus\": \"$pStatus\"" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).delete(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string())
                    callback(true, null)
                }
                else {
                    callback(false, null)
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Update User Prefs

    fun updateUserPrefs(userId: Int, prefs: UserPrefs, callback: (Boolean, String?) -> Unit) {
        val path = "/users/prefs/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"userId\": $userId,\n" +
                "    \"isBeginner\": ${prefs.beginner},\n" +
                "    \"isIntermediate\": ${prefs.intermediate},\n" +
                "    \"isExpert\": ${prefs.expert},\n" +
                "    \"isScentful\": ${prefs.scented},\n" +
                "    \"isPetFriendly\": ${prefs.petFriendly},\n" +
                "    \"isCleansAir\": ${prefs.cleanAir},\n" +
                "    \"isColorful\": ${prefs.colorful}" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string())
                    callback(true, null)
                }
                else {
                    callback(false, null)
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }

    // Watered Plant

    fun wateredPlant(id: Int, uid: String, callback: (Boolean) -> Unit) {
        val path = "/users/myPlants/watered/"
        val url = "$baseUrl$path"
        val json = "" +
                "{\n" +
                "    \"id\": $id,\n" +
                "    \"userId\": \"$uid\"" +
                "\n}"
        val body = json.toRequestBody(JSON)
        val request = Request.Builder().url(url).post(body).build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false)
                Global.logEvent("Response Failed", hashMapOf("API" to path))
                call.cancel()
            }
            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful) {
                    val jsonObject = JSONObject(response.body!!.string())
                    callback(true)
                }
                else {
                    callback(false)
                    Global.logEvent("Response Failed", hashMapOf("API" to path, "JSON" to response.body.toString(), "Code" to response.code.toString()))
                }
            }
        })
    }
}