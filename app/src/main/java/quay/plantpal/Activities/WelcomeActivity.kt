package quay.plantpal.Activities

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import quay.plantpal.Global
import quay.plantpal.PlantPalAPI
import quay.plantpal.R
import java.util.regex.Pattern

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        checkAppVersion()
        val signInButton = findViewById<Button>(R.id.sign_in_button)
        val createAccountButton = findViewById<Button>(R.id.create_account_button)

        signInButton.setOnClickListener {
            val intent = Intent(baseContext, LoginActivity::class.java)
            startActivity(intent)
        }

        createAccountButton.setOnClickListener {
            val intent = Intent(baseContext, CreateAccountActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        checkAppVersion()
    }

    private fun checkAppVersion() {
        PlantPalAPI.setVersion(false) { success, version, storeLink ->
            runOnUiThread{
                if (success && version != "") {
                    if (isOutDated(version!!)) {
                        val dialogBuilder = AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                        dialogBuilder.setTitle("App Outdated")
                                .setMessage("Your app is outdated. Please update to the latest version")
                                .setCancelable(false)
                                .setPositiveButton("Update") { dialog, id ->
                                    val appPackageName = packageName // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                                    } catch (anfe: ActivityNotFoundException) {
                                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                                    }
                                }
                        dialogBuilder.create().show()
                    }
                }
            }
        }
    }

    private fun isOutDated(version: String): Boolean {
        val databaseVersion = matches(version)
        val localVersion = matches(Global.appVersion)
        for (index in databaseVersion.indices) {
            if (index < localVersion.size) {
                try {
                    val databaseValue = databaseVersion[index]
                    val localValue = localVersion[index]
                    if (databaseValue > localValue) {
                        return true
                    }
                    else if (localValue > databaseValue) {
                        return false
                    }
                }
                catch (e: Exception) {
                    return false
                }
            }
            else {
                return false
            }
        }
        return false
    }

    private fun matches(version: String): ArrayList<Int> {
        val versionArray = arrayListOf<Int>()
        val regex = "([0-9]+)"
        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(version)
        while (matcher.find()) {
            versionArray.add(matcher.group().toInt())
        }
        return versionArray
    }
}
