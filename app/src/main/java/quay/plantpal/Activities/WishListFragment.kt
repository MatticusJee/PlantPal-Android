package quay.plantpal.Activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import coil.api.load
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread
import de.hdodenhof.circleimageview.CircleImageView
import quay.plantpal.Global
import quay.plantpal.Model.Plant
import quay.plantpal.PlantPalAPI
import quay.plantpal.R
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt
import uk.co.samuelwall.materialtaptargetprompt.extras.backgrounds.RectanglePromptBackground
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal


@SuppressLint("InflateParams")
class WishListFragment : Fragment() {

    private lateinit var mView: View
    private lateinit var wishListEmptyView: View
    private lateinit var wishListNotEmptyView: View
    private lateinit var wishlistGridView: GridView
    private lateinit var adapter: WishListFragment.WishListGridviewAdapter
    private var wishListPlants = arrayListOf<Plant>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mView = inflater.inflate(R.layout.activity_wishlist, container, false)
        configEmptyWishlistView()
        configNotEmptyWishlistView()
        getPlantData()
        return mView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200 && data != null) {
            if (requestCode == 121) {
                val tabValue = data.getIntExtra("tabValue", -1)
                val tabLayout = activity?.findViewById<TabLayout>(R.id.tab_layout)
                tabLayout?.getTabAt(tabValue)!!.select()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getPlantData()
    }

    private fun configEmptyWishlistView() {
        wishListEmptyView = mView.findViewById(R.id.empty_wishlist)
        val adView = mView.findViewById<AdView>(R.id.emptyWishListAdView)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun configNotEmptyWishlistView() {
        wishListNotEmptyView = mView.findViewById(R.id.wishlist)
        wishlistGridView = mView.findViewById(R.id.wishlist_collection)
        adapter = WishListGridviewAdapter(this)
        wishlistGridView.adapter = adapter
        wishlistGridView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val intent = Intent(mView.context, PlantDetailsActivity::class.java)
            intent.putExtra("plant", Gson().toJson(wishListPlants[position]))
            intent.putExtra("title", wishListPlants[position].pName)
            intent.putExtra("prevActivity", "WishList")
            startActivityForResult(intent, 121)
        }
        wishlistGridView.onItemLongClickListener = AdapterView.OnItemLongClickListener { adapterView, view, position, id ->
            val dialogBuilder = AlertDialog.Builder(mView.context, R.style.MyAlertDialogStyle)
            val plant = wishListPlants[position]
            dialogBuilder.setMessage("What do you want to do with this ${plant.pName}")
                    .setCancelable(true)
                    .setPositiveButton("Add to \"My Plants\"") { dialog, id ->
                        val congratsBuilder = AlertDialog.Builder(mView.context, R.style.MyAlertDialogStyle)
                                .setCancelable(false)
                                .setTitle("Congratulations! You added a plant! What are you gonna name it?")
                        val input = EditText(mView.context)
                        input.inputType = InputType.TYPE_CLASS_TEXT
                        congratsBuilder.setView(input)
                        congratsBuilder.setPositiveButton("OK") { dialog, id ->
                            val plantName = input.text.toString()
                            claimPlant(plant, plantName)
                        }
                        congratsBuilder.show()
                    }
                    .setNegativeButton("Remove") { dialog, id ->
                        removePlant(plant)
                    }
                    .setNeutralButton("Cancel") { dialog, id ->  }
            dialogBuilder.create().show()
            true
        }
        val adView = mView.findViewById<AdView>(R.id.notEmptyWishListAdView)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun getPlantData() {
        PlantPalAPI.getUserPlants(Global.user.uid, "WishList") { success, plants, error ->
            runOnUiThread{
                if(success) {
                    if(plants!!.isEmpty()) {
                        wishListEmptyView.visibility = View.VISIBLE
                        wishListNotEmptyView.visibility = View.GONE
                    }
                    else {
                        wishListEmptyView.visibility = View.GONE
                        wishListNotEmptyView.visibility = View.VISIBLE
                        wishListPlants = plants
                        wishListPlants.sortBy { it.userPlantId }
                        adapter.notifyDataSetChanged()
                        val sharedPreferencesEditor = context?.getSharedPreferences("UserActivityData", Context.MODE_PRIVATE)?.edit()
                        if (wishListPlants.size > 1) {
                            sharedPreferencesEditor?.putBoolean("HasSeenWishlistIntroduction", true)
                            sharedPreferencesEditor?.apply()
                        }
                    }
                }
                else {
                    Global.displayErrorAlert(mView.context, "Plant Pal Error", error)
                }
            }
        }
    }

    private fun claimPlant(plant: Plant, nickname: String) {
        PlantPalAPI.claimPlant(plant.userPlantId,
                                Global.user.uid,
                                plant.plantDbId!!,
                                plant.pStatus,
                                "MyPlants",
                                plant.pName!!,
                                nickname) { success, error ->
            runOnUiThread {
                if (success) {
                    Global.logEvent("Added Plant", hashMapOf("Section" to "MyPlants"))
                    val tabLayout = activity?.findViewById<TabLayout>(R.id.tab_layout)
                    tabLayout?.getTabAt(0)!!.select()
                }
                else {
                    Global.displayErrorAlert(mView.context, "Error", "Couldn't add plant to \"My Plants\". Please try again")
                }
            }
        }
    }

    private fun removePlant(plant: Plant) {
        PlantPalAPI.removePlant(plant.userPlantId!!, Global.user.uid, "WishList") { success, error ->
            runOnUiThread {
                if (success) {
                    getPlantData()
                    adapter.notifyDataSetChanged()
                    Global.logEvent("Removed Plant", hashMapOf("PlantId" to plant.plantDbId.toString(), "Section" to "WishList"))
                }
                else {
                    Global.displayErrorAlert(mView.context, "Removal Error", "Couldn't remove plant. Please try again")
                }
            }
        }
    }

    private fun showInstructions() {
        val mainThreadHandler = Handler( Looper.getMainLooper() )
        mainThreadHandler.postDelayed({
            MaterialTapTargetPrompt.Builder(this.requireActivity())
                    .setTarget(wishlistGridView.getChildAt(0))
                    .setPrimaryText("Tap for plant info")
                    .setSecondaryText("Hold to move to \"My Plants\" or remove plant")
                    .setPromptFocal(RectanglePromptFocal())
                    .setPromptBackground(RectanglePromptBackground())
                    .setBackgroundColour(ContextCompat.getColor(requireContext(), R.color.green))
                    .setCaptureTouchEventOutsidePrompt(false)
                    .setPromptStateChangeListener { prompt, state ->
                        if (state == MaterialTapTargetPrompt.STATE_REVEALED) {
                            val sharedPreferencesEditor = context?.getSharedPreferences("UserActivityData", Context.MODE_PRIVATE)?.edit()
                            sharedPreferencesEditor?.putBoolean("HasSeenWishlistIntroduction", true)
                            sharedPreferencesEditor?.apply()
                        }
                    }
                    .show()
        }, 500)
    }

    inner class WishListGridviewAdapter internal constructor(context: WishListFragment) : BaseAdapter() {

        private val layoutInflater: LayoutInflater = LayoutInflater.from(context.context)

        private var intstructionsCalled = false

        override fun getCount(): Int {
            // TODO Auto-generated method stub
            return wishListPlants.size
        }

        override fun getItem(position: Int): Plant {
            // TODO Auto-generated method stub
            return wishListPlants[position]
        }

        override fun getItemId(position: Int): Long {
            // TODO Auto-generated method stub
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            // TODO Auto-generated method stub
            val grid = convertView ?: layoutInflater.inflate(R.layout.wishlist_gridview, null)
            val imageView = grid.findViewById<CircleImageView>(R.id.image)
            val textView = grid.findViewById<TextView>(R.id.text)
            val matrix = ColorMatrix()
            matrix.setSaturation(0f)
            val filter = ColorMatrixColorFilter(matrix)
            imageView.colorFilter = filter
            val path = wishListPlants[position].imagePath
            imageView.load("https://plantpal.s3.amazonaws.com$path")
            textView.text = wishListPlants[position].pName
            val sharedPreferencesEditor = context?.getSharedPreferences("UserActivityData", Context.MODE_PRIVATE)
            val hasSeenIntroduction = sharedPreferencesEditor?.getBoolean("HasSeenWishlistIntroduction", false)
            if (position == 0 && hasSeenIntroduction == false && wishListPlants.size == 1 && !intstructionsCalled) {
                showInstructions()
                intstructionsCalled = true
            }
            return grid
        }
    }

    companion object {
        fun newInstance(): WishListFragment = WishListFragment()
    }
}

