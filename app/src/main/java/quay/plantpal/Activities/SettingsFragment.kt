package quay.plantpal.Activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.snapchat.kit.sdk.SnapLogin
import quay.plantpal.Global
import quay.plantpal.Model.LoginType
import quay.plantpal.PlantPalAPI
import quay.plantpal.R


@SuppressLint("InflateParams")
class SettingsFragment : Fragment() {

    private lateinit var mView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mView = inflater.inflate(R.layout.activity_settings, container, false)
        val listView = mView.findViewById<ListView>(R.id.settings_table_layout)
        val listViewAdapter = SettingsAdapter(this)
        listView.adapter = listViewAdapter
        listView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            if (position == 1) { //Privacy Policy
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://plantpal.s3.amazonaws.com/Website/privacy_policy.html"))
                startActivity(browserIntent)
                Global.logEvent("Viewing Privacy Policy")
            } else if (position == 3) { // Feedback
                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "plain/text"
                intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("plantpalapp@gmail.com"))
                intent.putExtra(Intent.EXTRA_SUBJECT, "Add/Edit Plant")
                startActivity(Intent.createChooser(intent, "Send Feedback"))
                Global.logEvent("Composing Feedback")
            }
        }
        return mView
    }

    inner class SettingsAdapter internal constructor(view: SettingsFragment): BaseAdapter() {

        private val layoutInflater: LayoutInflater = LayoutInflater.from(view.context)

        private var settingsInfo = arrayListOf<Pair<String, String>>()

        init {
            settingsInfo = getSettingsInfo()
        }

        override fun getCount(): Int {
            return settingsInfo.size
        }

        override fun getItem(position: Int): Any {
            return settingsInfo[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, view: View?, parent: ViewGroup?): View? {
            var row = view
            if (position == 0) {
                row = layoutInflater.inflate(R.layout.settings_account_rows, null)
                val accountHeaderRow = row.findViewById<View>(R.id.account_header)
                accountHeaderRow.setBackgroundColor(ContextCompat.getColor(mView.context, R.color.settings_background))
                val logoutButton = accountHeaderRow.findViewById<Button>(R.id.settings_logout)
                logoutButton.setOnClickListener {
                    val dialogBuilder = AlertDialog.Builder(mView.context, R.style.MyAlertDialogStyle)
                    dialogBuilder.setMessage("Are you sure you want to logout?")
                            .setCancelable(true)
                            .setPositiveButton("Yes") { dialog, id ->
                                Global.logEvent("Logged Out")
                                googleSignOut()
                                snapchatSignOut()
                                facebookSignOut()
                                val intent = Intent(context, WelcomeActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                startActivity(intent)
                                val sharedPreferences = context?.getSharedPreferences("UserData", Context.MODE_PRIVATE) ?: return@setPositiveButton
                                val token = sharedPreferences.getString("deviceToken", "") ?: return@setPositiveButton
                                removeDeviceToken(token)
                                val editor = sharedPreferences.edit()
                                editor.clear()
                                editor.apply()
                            }
                            .setNegativeButton("No") { dialog, id -> }
                    dialogBuilder.create().show()
                }
                val usernameRowLayout = row.findViewById<View>(R.id.username_row)
                val accountTypeLayout = row.findViewById<View>(R.id.account_type_row)
                val usernameTitle = usernameRowLayout.findViewById<TextView>(R.id.settings_username_title)
                val usernameValue = usernameRowLayout.findViewById<TextView>(R.id.settings_username_value)
                val accountTypeTitle = accountTypeLayout.findViewById<TextView>(R.id.settings_username_title)
                val accountTypeValue = accountTypeLayout.findViewById<TextView>(R.id.settings_username_value)
                usernameTitle.text = settingsInfo[0].first
                usernameValue.text = settingsInfo[0].second
                accountTypeTitle.text = settingsInfo[1].first
                accountTypeValue.text = settingsInfo[1].second
            }
            else if (position == 1 || position == 2) {
                row = layoutInflater.inflate(R.layout.settings_key_value_row, null)
                val title = row.findViewById<TextView>(R.id.settings_username_title)
                val value = row.findViewById<TextView>(R.id.settings_username_value)
                title.text = settingsInfo[position + 1].first
                value.text = settingsInfo[position + 1].second
            }
            else if (position == 3) {
                row = layoutInflater.inflate(R.layout.settings_feedback_row, null)
            }
            return row
        }
    }

    private fun getSettingsInfo() : ArrayList<Pair<String, String>> {
        val info = arrayListOf<Pair<String, String>>()
        if (Global.user.loginType == LoginType.plantpal || Global.user.loginType == LoginType.snapchat) {
            info.add("Username" to Global.user.username)
        }
        else {
            info.add("Email" to Global.user.email)
        }
        info.add("Account Type" to Global.user.loginType.type)
        info.add("Privacy Policy" to "")
        info.add("App Version" to Global.appVersion)
        return info
    }

    private fun removeDeviceToken(token: String) {
        PlantPalAPI.removeDeviceToken(Global.user.uid, token) { success, error -> }
    }

    private fun googleSignOut() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build()
        val googleSignInClient = GoogleSignIn.getClient(mView.context, gso)
        googleSignInClient.signOut()
    }

    private fun snapchatSignOut() {
        SnapLogin.getAuthTokenManager(mView.context).revokeToken()
    }

    private fun facebookSignOut() {
        LoginManager.getInstance().logOut()
    }

    companion object {
        fun newInstance(): SettingsFragment = SettingsFragment()
    }
}