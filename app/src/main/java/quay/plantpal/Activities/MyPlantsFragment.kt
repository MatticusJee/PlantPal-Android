package quay.plantpal.Activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import coil.api.load
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.gson.Gson
import com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread
import com.tapadoo.alerter.Alerter
import de.hdodenhof.circleimageview.CircleImageView
import quay.plantpal.Global
import quay.plantpal.Model.Plant
import quay.plantpal.PlantPalAPI
import quay.plantpal.R
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt
import uk.co.samuelwall.materialtaptargetprompt.extras.backgrounds.RectanglePromptBackground
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal


@SuppressLint("InflateParams")
class MyPlantsFragment : Fragment() {

    private lateinit var mView: View
    private lateinit var myPlantsEmptyView: View
    private lateinit var myPlantsNotEmptyView: View
    private lateinit var myPlantsGridView: GridView
    private lateinit var adapter: MyPlantsGridviewAdapter
    internal val statusIndicator = arrayOf("Happy" to R.color.blue, "Not Bad" to R.color.yellow, "Sad" to R.color.red)
    private var myPlants = arrayListOf<Plant>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mView = inflater.inflate(R.layout.activity_my_plants, container, false)
        configEmptyMyPlantsView()
        configNotEmptyMyPlantsView()
        getPlantData()
        return mView
    }

    override fun onResume() {
        super.onResume()
        getPlantData()
    }

    private fun configEmptyMyPlantsView() {
        myPlantsEmptyView = mView.findViewById(R.id.empty_my_plants)
        val adView = mView.findViewById<AdView>(R.id.emptyMyPlantsAdView)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun configNotEmptyMyPlantsView() {
        myPlantsNotEmptyView = mView.findViewById(R.id.my_plants)
        val statusIndicatorGridView = mView.findViewById<GridView>(R.id.status_indicator_gridview)
        val statusAdapter = StatusAdapter(this)
        statusIndicatorGridView.adapter = statusAdapter
        myPlantsGridView = mView.findViewById(R.id.my_plants_collection)
        adapter = MyPlantsGridviewAdapter(this)
        myPlantsGridView.adapter = adapter
        myPlantsGridView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val intent = Intent(mView.context, PlantDetailsActivity::class.java)
            intent.putExtra("plant", Gson().toJson(myPlants[position]))
            intent.putExtra("title", myPlants[position].pNickName)
            intent.putExtra("prevActivity", "MyPlants")
            startActivity(intent)
        }
        myPlantsGridView.onItemLongClickListener = AdapterView.OnItemLongClickListener { adapterView, view, position, id ->
            val dialogBuilder = AlertDialog.Builder(mView.context, R.style.MyAlertDialogStyle)
            val plant = myPlants[position]
            dialogBuilder.setMessage("What do you want to do with ${plant.pNickName}?")
                    .setCancelable(true)
                    .setPositiveButton("Water ${plant.pNickName}") { dialog, id ->
                        waterPlant(plant)
                    }
                    .setNegativeButton("Remove") { dialog, id ->
                        removePlant(plant)
                    }
                    .setNeutralButton("Cancel") { dialog, id ->  }
//            dialogBuilder..setBackgroundDrawable(ColorDrawable(Color.YELLOW));
            dialogBuilder.create().show()
            true
        }
        val adView = mView.findViewById<AdView>(R.id.notEmptyMyPlantsAdView)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun getPlantData() {
        PlantPalAPI.getUserPlants(Global.user.uid, "MyPlants") { success, plants, error ->
            runOnUiThread{
                if(success) {
                    if(plants!!.isEmpty()) {
                        myPlantsEmptyView.visibility = View.VISIBLE
                        myPlantsNotEmptyView.visibility = View.GONE
                    }
                    else {
                        myPlantsEmptyView.visibility = View.GONE
                        myPlantsNotEmptyView.visibility = View.VISIBLE
                        myPlants = plants
                        myPlants.sortBy { it.userPlantId }
                        adapter.notifyDataSetChanged()
                        val sharedPreferencesEditor = context?.getSharedPreferences("UserActivityData", Context.MODE_PRIVATE)?.edit()
                        if (myPlants.size > 1) {
                            sharedPreferencesEditor?.putBoolean("HasSeenMyPlantsIntroduction", true)
                            sharedPreferencesEditor?.apply()
                        }
                    }
                }
                else {
                    Global.displayErrorAlert(mView.context, "Plant Pal Error", error)
                }
            }
        }
    }

    private fun waterPlant(plant: Plant) {
        PlantPalAPI.wateredPlant(plant.userPlantId!!, Global.user.uid) { success ->
            runOnUiThread {
                if (success) {
                    getPlantData()
                    adapter.notifyDataSetChanged()
                    Global.logEvent("Watered Plant", hashMapOf("PlantId" to plant.plantDbId.toString()))
                    Alerter.create(activity)
                            .setText("${plant.pNickName} is watered!")
                            .setBackgroundColorRes(R.color.blue)
                            .enableSwipeToDismiss()
                            .show()
                }
                else {
                    Global.displayErrorAlert(mView.context, "Hydration Error", "Couldn't water ${plant.pNickName}. Please try again")
                }
            }
        }
    }

    private fun removePlant(plant: Plant) {
        PlantPalAPI.removePlant(plant.userPlantId!!, Global.user.uid, "MyPlants") { success, error ->
            runOnUiThread {
                if (success) {
                    runOnUiThread {
                        getPlantData()
                        adapter.notifyDataSetChanged()
                        Global.logEvent("Removed Plant", hashMapOf("PlantId" to plant.plantDbId.toString(), "Section" to "MyPlants"))
                    }
                }
                else {
                    Global.displayErrorAlert(mView.context, "Removal Error", "Couldn't remove plant. Please try again")
                }
            }
        }
    }

    private fun showInstructions() {
        MaterialTapTargetPrompt.Builder(this.requireActivity())
                .setTarget(myPlantsGridView.getChildAt(0))
                .setPrimaryText("Tap for plant info")
                .setSecondaryText("Hold to water or remove plant")
                .setPromptFocal(RectanglePromptFocal())
                .setPromptBackground(RectanglePromptBackground())
                .setBackgroundColour(ContextCompat.getColor(requireContext(), R.color.green))
                .setCaptureTouchEventOutsidePrompt(false)
                .setPromptStateChangeListener { prompt, state ->
                    if (state == MaterialTapTargetPrompt.STATE_REVEALED) {
                        val sharedPreferencesEditor = context?.getSharedPreferences("UserActivityData", Context.MODE_PRIVATE)?.edit()
                        sharedPreferencesEditor?.putBoolean("HasSeenMyPlantsIntroduction", true)
                        sharedPreferencesEditor?.apply()
                    }
                }
                .show()
    }

    inner class StatusAdapter internal constructor(view: MyPlantsFragment) : BaseAdapter() {

        private val layoutInflater: LayoutInflater = LayoutInflater.from(view.context)

        override fun getCount(): Int {
            return statusIndicator.size
        }

        override fun getItem(i: Int): Any {
            return statusIndicator[i].first
        }

        override fun getItemId(i: Int): Long {
            return i.toLong()
        }

        override fun areAllItemsEnabled(): Boolean {
            return false
        }

        override fun isEnabled(position: Int): Boolean {
            return false
        }

        override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
            val grid = view ?: layoutInflater.inflate(R.layout.plant_status_gridview, null)
            val statusIcon = grid.findViewById<View>(R.id.status_indicator_view)
            val textView = grid.findViewById<TextView>(R.id.text)
            val circle = statusIcon.background
            val drawable = circle as GradientDrawable
            drawable.setColor(ContextCompat.getColor(mView.context, statusIndicator[i].second))
            textView.text = statusIndicator[i].first
            return grid
        }
    }

    inner class MyPlantsGridviewAdapter internal constructor(context: MyPlantsFragment) : BaseAdapter() {

        private val layoutInflater: LayoutInflater = LayoutInflater.from(context.context)

        override fun getCount(): Int {
            return myPlants.size
        }

        override fun getItem(position: Int): Plant {
            return myPlants[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val grid = convertView ?: layoutInflater.inflate(R.layout.my_plants_gridview, null)
            val imageView = grid.findViewById<CircleImageView>(R.id.image)
            val textView = grid.findViewById<TextView>(R.id.text)
            imageView.borderColor = Color.parseColor(myPlants[position].wateredStatus?.color)
            val path = myPlants[position].imagePath
            imageView.load("https://plantpal.s3.amazonaws.com$path")
            textView.text = myPlants[position].pNickName
            val sharedPreferencesEditor = context?.getSharedPreferences("UserActivityData", Context.MODE_PRIVATE)
            val hasSeenIntroduction = sharedPreferencesEditor?.getBoolean("HasSeenMyPlantsIntroduction", false)
            if (position == 0 && hasSeenIntroduction == false) {
                showInstructions()
            }
            return grid
        }
    }

    companion object {
        fun newInstance(): MyPlantsFragment = MyPlantsFragment()
    }
}