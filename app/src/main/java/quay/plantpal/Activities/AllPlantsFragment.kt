package quay.plantpal.Activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import coil.api.load
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread
import quay.plantpal.Global
import quay.plantpal.Model.Plant
import quay.plantpal.Model.UserPrefs
import quay.plantpal.PlantPalAPI
import quay.plantpal.R


@SuppressLint("InflateParams")
class AllPlantsFragment : Fragment() {

    private lateinit var mView: View
    lateinit var gridView: GridView
    private lateinit var listView: ListView
    private lateinit var listViewAdapter: AllPlantsListViewAdapter
    private var plantDbOfLastPlantOnList: Int? = null
    private var isFetchingData = false
    lateinit var userPrefs: UserPrefs
    private var prefsToDisplay = arrayListOf<Pair<Int, String>>()

    private var numOfPrefs = 0
    private var currentGridViewPosition = 0
    private var isNewSearch = true
    private var searchString = ""

    private var rows = arrayListOf<AllPlantsListViewRow>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mView = inflater.inflate(R.layout.activity_all_plants, container, false)
        val searchBar = mView.findViewById<EditText>(R.id.searchBar)
        searchBar.addTextChangedListener { search ->
            isNewSearch = true
            searchString = search.toString()
            plantDbOfLastPlantOnList = null
            getAvailablePlants(searchString)
        }
        gridView = mView.findViewById(R.id.plant_pref_gridview)
        listView = mView.findViewById(R.id.all_plants_listView)
        listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            if (rows.isNotEmpty()) {
                val intent = Intent(mView.context, PlantDetailsActivity::class.java)
                intent.putExtra("plant", Gson().toJson(rows[position].thisPlant))
                intent.putExtra("title", rows[position].thisPlant?.pName)
                intent.putExtra("prevActivity", "AllPlants")
                startActivityForResult(intent, 120)
            }
        }
        val options = mView.findViewById<FloatingActionButton>(R.id.all_plants_options)
        options.setOnClickListener {
            val intent = Intent(mView.context, SelectFiltersActivity::class.java)
            intent.putExtra("userPreferences", Gson().toJson(prefsToDisplay))
            startActivityForResult(intent, 1142)
        }
        listView.setOnScrollListener(object : AbsListView.OnScrollListener {
            override fun onScrollStateChanged(view: AbsListView, scrollState: Int) { }
            override fun onScroll(view: AbsListView, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
                if (firstVisibleItem != 0 && visibleItemCount != 0 && totalItemCount != 0) {
                    if (firstVisibleItem + visibleItemCount >= totalItemCount && !isFetchingData && plantDbOfLastPlantOnList != null) {
                        isNewSearch = false
                        getAvailablePlants(searchString)
                    }
                }
            }
        })
        val leftButton = mView.findViewById<Button>(R.id.leftButton)
        val rightButton = mView.findViewById<Button>(R.id.rightButton)
        leftButton.setOnClickListener {
            currentGridViewPosition = if (currentGridViewPosition - 3 >= -1) currentGridViewPosition - 3 else 0
            gridView.smoothScrollToPosition(currentGridViewPosition)
        }
        rightButton.setOnClickListener {
            currentGridViewPosition = if (currentGridViewPosition + 3 <= numOfPrefs) currentGridViewPosition + 3 else numOfPrefs
            gridView.smoothScrollToPosition(currentGridViewPosition)
        }
        return mView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200 && data != null) {
            if (requestCode == 120) {
                val tabValue = data.getIntExtra("tabValue", -1)
                val tabLayout = activity?.findViewById<TabLayout>(R.id.tab_layout)
                tabLayout?.getTabAt(tabValue)!!.select()
            }
            else if (requestCode == 1142) {
                getAvailablePlants()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isNewSearch) {
            getAvailablePlants(searchString)
        }
    }

    @SuppressLint("InflateParams")
    private fun getAvailablePlants(searchString: String = "") {
        isFetchingData = true
        PlantPalAPI.availablePlants(Global.user.uid.toInt(), searchString,  lastPlantLoaded = plantDbOfLastPlantOnList) { success, plantRows, lastEvaluatedPlantIdKey, userPrefs, error ->
            runOnUiThread {
                if (success) {
                    if (isNewSearch) {
                        rows = plantRows!!
                        listViewAdapter = AllPlantsListViewAdapter(rows)
                        listView.adapter = listViewAdapter
                        isNewSearch = false
                    }
                    else if (plantRows!!.size >= 1 && plantRows[0].thisPlant != null && plantDbOfLastPlantOnList != null) {
                        rows.addAll(plantRows)
                        listViewAdapter.notifyDataSetChanged()
                    }
                    plantDbOfLastPlantOnList = lastEvaluatedPlantIdKey
                    this.userPrefs = userPrefs!!
                    val userFiltersAdapter = UserFiltersViewAdapter(this)
                    gridView.adapter = userFiltersAdapter
                    isFetchingData = false
                }
            }
        }
    }

    inner class UserFiltersViewAdapter internal constructor(view: AllPlantsFragment): BaseAdapter() {

        private val layoutInflater: LayoutInflater = LayoutInflater.from(view.context)
        private var bitmap = arrayOfNulls<Bitmap>(0)

        init {
            prefsToDisplay = getPrefsToDisplay()
            gridView.numColumns = if (prefsToDisplay.size >= 3) 3 else prefsToDisplay.size
            gridView.columnWidth = gridView.width
            when (prefsToDisplay.size) {
                1 -> {
                    gridView.columnWidth = gridView.width
                }
                2 -> {
                    gridView.columnWidth = gridView.width / 2
                }
                else -> {
                    gridView.columnWidth = gridView.width / 3
                }
            }
            bitmap = arrayOfNulls(prefsToDisplay.size)
        }

        override fun getCount(): Int {
            return prefsToDisplay.size
        }

        override fun getItemId(i: Int): Long {
            return i.toLong()
        }

        override fun getItem(i: Int): Any {
            return i
        }

        override fun getView(position: Int, view: View?, parent: ViewGroup): View {
            val table = view ?: layoutInflater.inflate(R.layout.all_plants_prefs_gridview, null)
            val imageView = table.findViewById<ImageView>(R.id.imageView)
            val textView = table.findViewById<TextView>(R.id.text)
            val options = BitmapFactory.Options()
            options.inScaled = false
            bitmap[position] = BitmapFactory.decodeResource(context?.resources, prefsToDisplay[position].first, options)
            imageView.setImageBitmap(bitmap[position])
            textView.text = prefsToDisplay[position].second
            return table
        }

        private fun getPrefsToDisplay(): ArrayList<Pair<Int, String>> {
            val prefsToDisplay = arrayListOf<Pair<Int, String>>()
            if (userPrefs.beginner) {
                prefsToDisplay.add(R.drawable.difficulty_1 to "BEGINNER")
            }
            if (userPrefs.intermediate) {
                prefsToDisplay.add(R.drawable.difficulty_2 to "INTERMEDIATE")
            }
            if (userPrefs.expert) {
                prefsToDisplay.add(R.drawable.difficulty_3 to "EXPERT")
            }
            if (userPrefs.cleanAir) {
                prefsToDisplay.add(R.drawable.clean_air to "CLEANS AIR")
            }
            if (userPrefs.petFriendly) {
                prefsToDisplay.add(R.drawable.safe_for_pets to "PET FRIENDLY")
            }
            if (userPrefs.colorful) {
                prefsToDisplay.add(R.drawable.colorful to "COLORFUL")
            }
            if (userPrefs.scented) {
                prefsToDisplay.add(R.drawable.scented to "SCENTED")
            }
            numOfPrefs = prefsToDisplay.size
            return prefsToDisplay
        }
    }

    inner class AllPlantsListViewAdapter internal constructor(rowAllPlants: ArrayList<AllPlantsListViewRow>): BaseAdapter() {

        private val rows = rowAllPlants

        override fun getCount(): Int {
            return rows.size
        }

        override fun getItem(position: Int): AllPlantsListViewRow {
            return rows[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, view: View?, parent: ViewGroup): View {
            val listViewRow = rows[position]
            val rowView = view ?: layoutInflater.inflate(listViewRow.layoutId, null)
            val xPlant = listViewRow.thisPlant.let { it!! }
            val imageView: ImageView = rowView.findViewById(R.id.imageView)
            val plantNameTextView: TextView = rowView.findViewById(R.id.plant_name_textview)
            val gridView: GridView = rowView.findViewById(R.id.plant_characteristics)
            val plantInfoTextView: TextView = rowView.findViewById(R.id.plant_peak_description)
            val path = xPlant.imagePath
            imageView.load("https://plantpal.s3.amazonaws.com$path") {
                placeholder(R.drawable.confused_spike)
            }
            plantNameTextView.text = xPlant.pName
            plantInfoTextView.text = xPlant.lightInfo
            gridView.onItemClickListener = listView.onItemClickListener
            gridView.adapter = PlantRowGridview(gridView, xPlant)
            return rowView
        }

        inner class PlantRowGridview internal constructor(gridview: GridView, plant: Plant): BaseAdapter() {

            private var plantCharacteristics = arrayListOf<Int>()

            init {
                plantCharacteristics = getCharacteristics(plant)
                gridview.numColumns = plantCharacteristics.size
            }

            override fun getCount(): Int {
                return plantCharacteristics.size
            }

            override fun getItem(p0: Int): Int {
                return plantCharacteristics[p0]
            }

            override fun getItemId(p0: Int): Long {
                return p0.toLong()
            }

            override fun getView(position: Int, view: View?, parent: ViewGroup): View {
                val gridView = view ?: layoutInflater.inflate(R.layout.all_plants_characteristic_gridview, null)
                val imageView = gridView.findViewById<ImageView>(R.id.characteristic_image)
                val options = BitmapFactory.Options()
                options.inScaled = false
                val bitmap = BitmapFactory.decodeResource(context?.resources, plantCharacteristics[position], options)
                imageView.setImageBitmap(bitmap)
                return gridView
            }

            private fun getCharacteristics(plant: Plant) : ArrayList<Int> {
                val array = arrayListOf<Int>()
                when (plant.difficulty) {
                    "Beginner" -> {
                        array.add(R.drawable.difficulty_1)
                    }
                    "Intermediate" -> {
                        array.add(R.drawable.difficulty_2)
                    }
                    "Expert" -> {
                        array.add(R.drawable.difficulty_3)
                    }
                }

                if (plant.isPetFriendly == true) {
                    array.add(R.drawable.safe_for_pets)
                }
                else {
                    array.add(R.drawable.not_safe_for_pets)
                }

                when {
                    plant.cleansAir == true -> {
                        array.add(R.drawable.clean_air)
                    }
                    plant.isColorful == true -> {
                        array.add(R.drawable.colorful)
                    }
                    plant.isScented == true -> {
                        array.add(R.drawable.scented)
                    }
                }
                return array
            }
        }
    }

    companion object {
        fun newInstance(): AllPlantsFragment = AllPlantsFragment()
    }
}

class AllPlantsListViewRow internal constructor(layoutId: Int, plant: Plant? = null) {

    val layoutId = layoutId
    var thisPlant: Plant? = null

    init {
        thisPlant = plant
    }
}

