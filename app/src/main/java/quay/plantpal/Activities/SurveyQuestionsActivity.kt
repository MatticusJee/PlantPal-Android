package quay.plantpal.Activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import quay.plantpal.Global
import quay.plantpal.Model.User
import quay.plantpal.Model.UserPrefs
import quay.plantpal.PlantPalAPI
import quay.plantpal.R

@SuppressLint("InflateParams")
class SurveyQuestionsActivity : Activity() {

    private var difficulty = arrayListOf(
            R.drawable.difficulty_1 to "BEGINNER",
            R.drawable.difficulty_2 to "INTERMEDIATE",
            R.drawable.difficulty_3 to "EXPERT"
    )

    private var characteristic = arrayListOf(
            R.drawable.clean_air to "CLEANS AIR",
            R.drawable.safe_for_pets to "PET FRIENDLY",
            R.drawable.colorful to "COLORFUL",
            R.drawable.scented to "SCENTED"
    )

    private var currentQuestionSelection = arrayListOf<Pair<Int, String>>()
    private var selectedPreferences = arrayListOf<Pair<Int, String>>()

    private var questionPage = 0

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey_questions)
        val intent = intent
        val arrayListType = object : TypeToken<ArrayList<Pair<Int, String>>>() {}.type
        questionPage = intent.getIntExtra("question_page", 0)
        if (intent.hasExtra("selected_preferences")) {
            selectedPreferences = Gson().fromJson(intent.getStringExtra("selected_preferences"), arrayListType)
        }
        val questionTextView = findViewById<TextView>(R.id.survey_question_textview)
        if (questionPage == 1) {
            questionTextView.text = "How experienced are you with planting?"
        }
        else {
            questionTextView.text = "What is your priority when finding a plant?"
        }
        val listView = findViewById<ListView>(R.id.survey_choices_listview)
        val adapter = SurveyAdapter(this)
        listView.adapter = adapter
        listView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val row = view ?: layoutInflater.inflate(R.layout.select_filters_listview_row, null)
            val circle = row.findViewById<ImageView>(R.id.filter_circle)
            if (currentQuestionSelection[position] in selectedPreferences) { // selected
                circle.background = ContextCompat.getDrawable(baseContext, R.drawable.white_custom_round_button)
                selectedPreferences.remove(currentQuestionSelection[position])
            }
            else {
                circle.background = ContextCompat.getDrawable(baseContext, R.drawable.green_custom_round_button)
                selectedPreferences.add(currentQuestionSelection[position])
            }
        }
        val stepsTextView = findViewById<TextView>(R.id.survey_steps_textview)
        stepsTextView.text = "Step $questionPage of 2"
        val backButton = findViewById<Button>(R.id.back_button)
        backButton.setOnClickListener {
            finish()
        }
        val nextButton = findViewById<Button>(R.id.next_button)
        nextButton.setOnClickListener {
            if (questionPage == 1) {
                val intent = Intent(this, SurveyQuestionsActivity::class.java)
                intent.putExtra("question_page", 2)
                intent.putExtra("selected_preferences", Gson().toJson(selectedPreferences))
                startActivity(intent)
            }
            else {
                val userPrefs = setUserPrefs()
                val sharedPreferences = getSharedPreferences("UserData", Context.MODE_PRIVATE)
                val userAsString = sharedPreferences.getString("user", null)
                val user = Gson().fromJson(userAsString, User::class.java)
                Global.user = user
                PlantPalAPI.updateUserPrefs(Global.user.uid.toInt(), prefs = userPrefs) { success, error ->
                    if (success) {
                        Global.prefsSet = userPrefs.isSet
                        val sharedPreferencesEditor = sharedPreferences.edit()
                        sharedPreferencesEditor.putBoolean("userPrefIsSet", userPrefs.isSet)
                        sharedPreferencesEditor.apply()
                        Global.logEvent("Survey Complete")
                        displayHomeActivity()
                    }
                    else {
                        Global.displayErrorAlert(this, "Plant Pal Error", error)
                    }
                }
            }
        }
    }

    private fun setUserPrefs(): UserPrefs {
        val userPrefs = UserPrefs() // Default everything is false
        userPrefs.isSet = true
        for (filter in selectedPreferences) {
            when (filter.second) {
                "BEGINNER" -> userPrefs.beginner = true
                "INTERMEDIATE" -> userPrefs.intermediate = true
                "EXPERT" -> userPrefs.expert = true
                "CLEANS AIR" -> userPrefs.cleanAir = true
                "PET FRIENDLY" -> userPrefs.petFriendly = true
                "COLORFUL" -> userPrefs.colorful = true
                "SCENTED" -> userPrefs.scented = true
            }
        }
        return userPrefs
    }

    private fun displayHomeActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    inner class SurveyAdapter internal constructor(context: SurveyQuestionsActivity): BaseAdapter() {

        private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

        override fun getCount(): Int {
            currentQuestionSelection = if (questionPage == 1) {
                difficulty
            } else {
                characteristic
            }
            return currentQuestionSelection.size
        }

        override fun getItem(position: Int): Any {
            return currentQuestionSelection[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, view: View?, parent: ViewGroup?): View? {
            val row = view ?: layoutInflater.inflate(R.layout.select_filters_listview_row, null)
            val circle = row.findViewById<ImageView>(R.id.filter_circle)
            val textView = row.findViewById<TextView>(R.id.filter_text)
            val plantImage = row.findViewById<ImageView>(R.id.plant_pref_image)
            circle.background = ContextCompat.getDrawable(baseContext, R.drawable.white_custom_round_button)
            textView.text = currentQuestionSelection[position].second
            plantImage.setImageDrawable(ContextCompat.getDrawable(baseContext, currentQuestionSelection[position].first))
            return row
        }
    }
}
