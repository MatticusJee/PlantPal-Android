package quay.plantpal.Activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import quay.plantpal.Global
import quay.plantpal.Model.UserPrefs
import quay.plantpal.PlantPalAPI
import quay.plantpal.R

@SuppressLint("InflateParams")
class SelectFiltersActivity : Activity() {

    private var totalFilters = arrayListOf(
            R.drawable.white_all_plants_icon to "ALL PLANTS",
            R.drawable.difficulty_1 to "BEGINNER",
            R.drawable.difficulty_2 to "INTERMEDIATE",
            R.drawable.difficulty_3 to "EXPERT",
            R.drawable.clean_air to "CLEANS AIR",
            R.drawable.safe_for_pets to "PET FRIENDLY",
            R.drawable.colorful to "COLORFUL",
            R.drawable.scented to "SCENTED"
    )

    private var filters = arrayListOf<Pair<Int, String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_filters)
        val intent = intent
        val arrayListType = object : TypeToken<ArrayList<Pair<Int, String>>>() {}.type
        filters = Gson().fromJson(intent.getStringExtra("userPreferences"), arrayListType)
        if (allFiltersSelected()) {
            filters.removeAll(filters)
            filters.add(totalFilters[0])
        }
        val listView = findViewById<ListView>(R.id.settings_table_layout)
        val adapter = FilterAdapter(this)
        listView.adapter = adapter
        listView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val row = view ?: layoutInflater.inflate(R.layout.select_filters_listview_row, null)
            val circle = row.findViewById<ImageView>(R.id.filter_circle)
            if (totalFilters[position] in filters && position != 0) { // selected
                circle.background = ContextCompat.getDrawable(baseContext, R.drawable.white_custom_round_button)
                filters.remove(totalFilters[position])
            }
            else {
                circle.background = ContextCompat.getDrawable(baseContext, R.drawable.green_custom_round_button)
                if (position == 0) {
                    filters.removeAll(filters)
                    filters.add(totalFilters[position])
                }
                else {
                    filters.remove(totalFilters[0])
                    filters.add(totalFilters[position])
                }
                adapter.notifyDataSetChanged()
            }
        }
        val findPlantsButton = findViewById<Button>(R.id.find_plants_button)
        findPlantsButton.setOnClickListener {
            if (filters.contains(totalFilters[0])) {
                setFiltersToAll()
            }
            val newPrefs = updateUserPrefs()
            PlantPalAPI.updateUserPrefs(Global.user.uid.toInt(), newPrefs) { success, error ->
                if (success) {
                    val intent = Intent()
                    intent.putExtra("newUserPrefsSet", true)
                    setResult(200, intent)
                    finish()
                }
                else {
                    Global.displayErrorAlert(this, "Plant Pal Error", error)
                }
            }
        }
    }

    private fun allFiltersSelected(): Boolean {
        if (filters.size == totalFilters.size - 1) {
            return true
        }
        return false
    }

    private fun setFiltersToAll() {
        filters.removeAll(filters)
        for (i in 1 until totalFilters.size) {
            filters.add(totalFilters[i])
        }
    }

    private fun updateUserPrefs(): UserPrefs {
        val userPrefs = UserPrefs() // Default everything is false
        userPrefs.isSet = true
        for (filter in filters) {
            when (filter.second) {
                "BEGINNER" -> userPrefs.beginner = true
                "INTERMEDIATE" -> userPrefs.intermediate = true
                "EXPERT" -> userPrefs.expert = true
                "CLEANS AIR" -> userPrefs.cleanAir = true
                "PET FRIENDLY" -> userPrefs.petFriendly = true
                "COLORFUL" -> userPrefs.colorful = true
                "SCENTED" -> userPrefs.scented = true
            }
        }
        return userPrefs
    }

    inner class FilterAdapter internal constructor(context: SelectFiltersActivity): BaseAdapter() {

        private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

        override fun getCount(): Int {
            return totalFilters.size
        }

        override fun getItem(position: Int): Any {
            return totalFilters[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, view: View?, parent: ViewGroup?): View? {
            val row = view ?: layoutInflater.inflate(R.layout.select_filters_listview_row, null)
            val circle = row.findViewById<ImageView>(R.id.filter_circle)
            val textView = row.findViewById<TextView>(R.id.filter_text)
            val plantImage = row.findViewById<ImageView>(R.id.plant_pref_image)
            if (totalFilters[position] in filters) {
                circle.background = ContextCompat.getDrawable(baseContext, R.drawable.green_custom_round_button)
            }
            else {
                circle.background = ContextCompat.getDrawable(baseContext, R.drawable.white_custom_round_button)
            }
            textView.text = totalFilters[position].second
            plantImage.setImageDrawable(ContextCompat.getDrawable(baseContext, totalFilters[position].first))
            return row
        }
    }
}
