package quay.plantpal.Activities

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.tabs.TabLayout
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import quay.plantpal.Global
import quay.plantpal.Model.User
import quay.plantpal.PlantPalAPI
import quay.plantpal.R
import java.util.regex.Pattern


@SuppressLint("InflateParams")
class MainActivity : AppCompatActivity() {

    private val tabIcons = arrayListOf(
            R.drawable.tab_my_plants_icon,
            R.drawable.tab_all_plants_icon,
            R.drawable.tab_wishlist_icon,
            R.drawable.tab_settings_icon
    )

    private val tabNames: HashMap<Int, String> = hashMapOf(
            0 to "My Plants",
            1 to "All Plants",
            2 to "Wish List",
            3 to "Settings"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_tabbed)
        MobileAds.initialize(this) {}
        AppCenter.start(application, "61b328a9-60cf-4d87-af82-a4c1cc4fff2d",
                Analytics::class.java, Crashes::class.java)
        val sharedPreferences = getSharedPreferences("UserData", Context.MODE_PRIVATE)
        val userAsString = sharedPreferences.getString("user", null)
        val userPrefsIsSet = sharedPreferences.getBoolean("userPrefIsSet", false)
        if (userAsString == "" || userAsString == null) {
            val intent = Intent(this, WelcomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
        else if (!userPrefsIsSet) {
            val user = Gson().fromJson(userAsString, User::class.java)
            Global.user = user
            val intent = Intent(this, StartSurveyActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
        else {
            val user = Gson().fromJson(userAsString, User::class.java)
            Global.user = user
            Global.prefsSet = userPrefsIsSet
            checkAppVersion()
            requestPushNotifications()
            Global.logEvent("User Opens/Logs into Application")
            val tabLayout: TabLayout = findViewById(R.id.tab_layout)
            val viewPager: ViewPager = findViewById(R.id.view_pager)
            val tabAdapter = TabAdapter(supportFragmentManager)
            viewPager.adapter = tabAdapter
            val sharedPreferencesEditor = getSharedPreferences("UserActivityData", Context.MODE_PRIVATE)
            val hasSeenWishListIntroduction = sharedPreferencesEditor?.getBoolean("HasSeenWishlistIntroduction", false)
            if (hasSeenWishListIntroduction == true) {
                viewPager.offscreenPageLimit = tabIcons.size - 1
            }
            else {
                viewPager.offscreenPageLimit = 1
            }
            tabLayout.setupWithViewPager(viewPager)
            tabLayout.tabMode = TabLayout.MODE_FIXED
            for (i in 0 until tabIcons.size) {
                val view = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null)
                val imageView = (view.findViewById(R.id.icon2) as ImageView)
                val textView = (view.findViewById(R.id.text1) as TextView)
                imageView.setImageResource(tabIcons[i])
                textView.text = tabNames[i]
                tabLayout.getTabAt(i)!!.customView = view
                if(i == 0) {
                    imageView.setColorFilter(ContextCompat.getColor(this, R.color.green))
                    textView.setTextColor(ContextCompat.getColor(this, R.color.bright_green))
                }
                else {
                    imageView.setColorFilter(ContextCompat.getColor(this, R.color.grey))
                }
            }
            listeners(tabLayout)
        }
    }

    private fun listeners(tabLayout: TabLayout) {

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                print("{${tab.text} selected")
                val imageView = tab.customView!!.findViewById<ImageView>(R.id.icon2)
                val textView = tab.customView!!.findViewById<TextView>(R.id.text1)
                imageView.setColorFilter(ContextCompat.getColor(baseContext, R.color.green))
                textView.setTextColor(ContextCompat.getColor(baseContext, R.color.bright_green))
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                print("{${tab.text} unselected")
                val imageView = tab.customView!!.findViewById<ImageView>(R.id.icon2)
                val textView = tab.customView!!.findViewById<TextView>(R.id.text1)
                imageView.setColorFilter(ContextCompat.getColor(baseContext, R.color.grey))
                textView.setTextColor(ContextCompat.getColor(baseContext, R.color.grey))
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                print("{${tab.text} reselected")
            }
        })
    }

    private fun checkAppVersion() {
        PlantPalAPI.setVersion(true) { success, version, storeLink ->
            runOnUiThread{
                if (success && version != "") {
                    if (isOutDated(version!!)) {
                        val dialogBuilder = AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                        dialogBuilder.setTitle("App Outdated")
                                .setMessage("Your app is outdated. Please update to the latest version")
                                .setCancelable(false)
                                .setPositiveButton("Update") { dialog, id ->
                                    val appPackageName = packageName // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                                    } catch (anfe: ActivityNotFoundException) {
                                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                                    }
                                }
                        dialogBuilder.create().show()
                    }
                }
            }
        }
    }

    private fun isOutDated(version: String): Boolean {
        val databaseVersion = matches(version)
        val localVersion = matches(Global.appVersion)
        for (index in databaseVersion.indices) {
            if (index < localVersion.size) {
                try {
                    val databaseValue = databaseVersion[index]
                    val localValue = localVersion[index]
                    if (databaseValue > localValue) {
                        return true
                    }
                    else if (localValue > databaseValue) {
                        return false
                    }
                }
                catch (e: Exception) {
                    return false
                }
            }
            else {
                return false
            }
        }
        return false
    }

    private fun matches(version: String): ArrayList<Int> {
        val versionArray = arrayListOf<Int>()
        val regex = "([0-9]+)"
        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(version)
        while (matcher.find()) {
            versionArray.add(matcher.group().toInt())
        }
        return versionArray
    }

    private fun requestPushNotifications() {
        FirebaseApp.initializeApp(this)
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "getInstanceId failed", task.exception)
                return@OnCompleteListener
            }
            // Get new Instance ID token
            val token = task.result?.token ?: return@OnCompleteListener
            //TODO: Save token
            PlantPalAPI.registerDeviceToken(Global.user.uid, token) { success, error ->
                if (success) {
                    print("Success")
                    val editor = getSharedPreferences("UserData", Context.MODE_PRIVATE).edit()
                    editor.putString("deviceToken", token)
                    editor.apply()
                }
                else {
                    print("Failed")
                }
            }
        })
    }

    inner class TabAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT ) {

        private val tabNames: HashMap<Int, String> = hashMapOf(
                0 to "My Plants",
                1 to "All Plants",
                2 to "Wish List",
                3 to "Settings"
        )

        private val fragments: HashMap<Int, Any> = hashMapOf(
                0 to MyPlantsFragment.newInstance(),
                1 to AllPlantsFragment.newInstance(),
                2 to WishListFragment.newInstance(),
                3 to SettingsFragment.newInstance()
        )

        override fun getItem(position: Int): Fragment {
            return fragments[position] as Fragment
        }

        override fun getPageTitle(position: Int): CharSequence = when (position) {
            position -> tabNames[position].toString()
            else -> ""
        }

        override fun getCount(): Int {
            return tabNames.count()
        }
    }
}