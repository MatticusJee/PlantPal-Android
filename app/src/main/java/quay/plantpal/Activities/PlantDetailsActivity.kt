package quay.plantpal.Activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.InputType
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import coil.api.load
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import quay.plantpal.Global
import quay.plantpal.Model.Plant
import quay.plantpal.PlantPalAPI
import quay.plantpal.R


@SuppressLint("InflateParams")
class PlantDetailsActivity : Activity() {

    enum class PreviousPlantPage {
        MyPlants,
        AllPlants,
        WishList
    }

    private lateinit var gridview: GridView
    private lateinit var options: FloatingActionButton

    private var userPlantId: Int? = null
    private var plantId: Int = -1
    private var currentPlantStatus: String? = null
    private lateinit var plantSelected: Plant
    private lateinit var plantDetails: Plant
    private var title: String = ""
    private var prevActivity: PreviousPlantPage? = null
    private var isViewLoaded = false

    private var careInfo: SpannableString? = null
    private var foodInfo: SpannableString? = null
    private var faqInfo: String = ""

    private lateinit var interstitialAd: InterstitialAd

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plant_details)
        val intent = intent
        title = intent.getStringExtra("title")
        val activityString = intent.getStringExtra("prevActivity")
        prevActivity = PreviousPlantPage.valueOf(activityString)
        plantSelected = Gson().fromJson(intent.getStringExtra("plant"), Plant::class.java)
        plantId = plantSelected.plantDbId!!
        if(plantSelected.pStatus != null && plantSelected.userPlantId != null) {
            currentPlantStatus = plantSelected.pStatus
            userPlantId = plantSelected.userPlantId
        }
        options = findViewById(R.id.plant_detail_options)
        options.isEnabled = false
        if (prevActivity == PreviousPlantPage.MyPlants) {
            options.hide()
        }
        options.setOnClickListener {
            if (prevActivity == PreviousPlantPage.AllPlants) {
                displayAllPlantsDialog()
            }
            else if (prevActivity == PreviousPlantPage.WishList) {
                displayWishlistDialog()
            }
        }
        val textView = findViewById<TextView>(R.id.textView2)
        val careButton = findViewById<RadioButton>(R.id.care)
        val foodButton = findViewById<RadioButton>(R.id.food)
        val faqButton = findViewById<RadioButton>(R.id.faq)
        if (!isViewLoaded) {
            careButton.performClick()
        }
        careButton.setOnClickListener {
            textView.text = careInfo
        }
        foodButton.setOnClickListener {
            textView.text = foodInfo
        }
        faqButton.setOnClickListener {
            textView.text = faqInfo
        }
        getPlantInfo()
        val adRequest = AdRequest.Builder().build()
        interstitialAd = InterstitialAd(this)
        interstitialAd.adUnitId = "ca-app-pub-6295152358301338/8453577344"
        interstitialAd.loadAd(adRequest)
    }

    private fun getPlantInfo() {
        val textView = findViewById<TextView>(R.id.textView4)
        val imageView = findViewById<ImageView>(R.id.imageView2)
        PlantPalAPI.getPlantInfo(plantId) { success, plant, error ->
            runOnUiThread {
                if (success) {
                    this.plantDetails = plant!!
                    textView.text = title
                    val path = plant.imagePath
                    imageView.load("https://plantpal.s3.amazonaws.com$path")
                    displayData()
                    options.isEnabled = true
                    gridview = findViewById(R.id.plant_detail_characteristics)
                    val adapter = PlantDetailsGridviewAdapter(this, this.plantDetails)
                    gridview.adapter = adapter
                }
                else {
                    Global.displayErrorAlert(this, "Plant Pal Error", error)
                }
            }
        }
    }

    private fun displayData() {
        val textView = findViewById<TextView>(R.id.textView2)
        careInfo = careInfoFormat()
        foodInfo = foodInfoFormat()
        faqInfo = if (plantDetails.faq != null && plantDetails.faq != "") {
            plantDetails.faq!!
        }
        else {
            "FAQ information not available"
        }
        if (!isViewLoaded) {
            textView.text = careInfo
        }
        isViewLoaded = true
    }

    private fun careInfoFormat(): SpannableString {
        val string = "LIGHT\n" + plantDetails.lightInfo + "\n\n" + "TEMPERATURE\n" + plantDetails.temp
        val tempIndex = string.indexOf("TEMPERATURE")
        val careString = SpannableString(string)
        careString.setSpan(StyleSpan(Typeface.BOLD), 0, "LIGHT".length, 0)
        careString.setSpan(StyleSpan(Typeface.BOLD), tempIndex, tempIndex + "TEMPERATURE".length, 0)
        return careString
    }

    private fun foodInfoFormat(): SpannableString {
        val string = "FERTILIZER\n" + plantDetails.fertilizerInfo + "\n\n" + "SOIL\n" + plantDetails.soilInfo
        val soilIndex = string.indexOf("SOIL")
        val careString = SpannableString(string)
        careString.setSpan(StyleSpan(Typeface.BOLD), 0, "FERTILIZER".length, 0)
        careString.setSpan(StyleSpan(Typeface.BOLD), soilIndex, soilIndex + "SOIL".length, 0)
        return careString
    }

    private fun displayAllPlantsDialog() {
        val dialogBuilder = AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
        dialogBuilder.setTitle("Woohoo!")
                .setMessage("Would you like to add the ${plantSelected.pName} to \"My Plants\" or save it to \"Wish List\"?")
                .setCancelable(true)
                .setPositiveButton("Add to \"My Plants\"") { dialog, id ->
                    val congratsBuilder = AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                            .setCancelable(false)
                            .setTitle("Congratulations! You added a plant! What are you gonna name it?")
                    val input = EditText(this)
                    input.inputType = InputType.TYPE_CLASS_TEXT
                    congratsBuilder.setView(input)
                    congratsBuilder.setPositiveButton("OK") { dialog, id ->
                        val plantName = input.text.toString()
                        if (plantName != "") {
                            claimPlant(plantSelected, "MyPlants", plantName)
                        } else {
                            Global.displayErrorAlert(this, "Nickname Error", "Please enter a name for your plant")
                        }
                    }
                    congratsBuilder.show()
                }
                .setNegativeButton("Add to \"Wishlist\"") { dialog, id ->
                    claimPlant(plantSelected, "WishList")
                }
                .setNeutralButton("Cancel") { dialog, id ->  }
        dialogBuilder.create().show()
    }

    private fun displayWishlistDialog() {
        val dialogBuilder = AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
        dialogBuilder.setTitle("Move plant to \"My Plants\"?")
                .setMessage("You're about to move this plant to \"My Plants\". Are you sure you want to do that?")
                .setCancelable(true)
                .setPositiveButton("Yes") { dialog, id ->
                    val congratsBuilder = AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                            .setCancelable(false)
                            .setTitle("Congratulations! You added a plant! What are you gonna name it?")
                    val input = EditText(this)
                    input.inputType = InputType.TYPE_CLASS_TEXT
                    congratsBuilder.setView(input)
                    congratsBuilder.setPositiveButton("OK") { dialog, id ->
                        val plantName = input.text.toString()
                        claimPlant(plantSelected, "MyPlants", plantName)
                    }
                    congratsBuilder.show()
                }
                .setNegativeButton("No") { dialog, id -> }
        dialogBuilder.create().show()
    }

    private fun claimPlant(plant: Plant, newStatus: String, nickname: String? = null) {
        PlantPalAPI.claimPlant(plant.userPlantId, Global.user.uid, plant.plantDbId!!, plant.pStatus, newStatus, plant.pName!!, nickname) { success, error ->
            runOnUiThread {
                if (success) {
                    Global.logEvent("Added Plant", hashMapOf("Section" to newStatus))
                    interstitialAd.show()
                    val tabValue = if (newStatus == "MyPlants") 0 else 2
                    val intent = Intent()
                    intent.putExtra("tabValue", tabValue)
                    setResult(200, intent)
                    finish()
                }
                else {
                    Global.displayErrorAlert(this, "Error", "Couldn't add plant. Please try again")
                }
            }
        }
    }

    inner class PlantDetailsGridviewAdapter internal constructor(context: PlantDetailsActivity, private var plantDetails: Plant) : BaseAdapter() {

        private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        private var characteristics = arrayListOf<Int>()

        init {
            characteristics = getCharacteristics()
        }

        override fun getCount(): Int {
            // TODO Auto-generated method stub
            return characteristics.count()
        }

        override fun getItem(position: Int): Int {
            // TODO Auto-generated method stub
            return characteristics[position]
        }

        override fun getItemId(position: Int): Long {
            // TODO Auto-generated method stub
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            // TODO Auto-generated method stub
            val gridViewItem = convertView ?: layoutInflater.inflate(R.layout.plant_details_gridview, null)
            val imageView = gridViewItem.findViewById<ImageView>(R.id.plant_details_image)
            imageView.setImageDrawable(getDrawable(characteristics[position]))
            return gridViewItem
        }

        private fun getCharacteristics() : ArrayList<Int> {
            val array = arrayListOf<Int>()
            when (plantDetails.difficulty) {
                "Beginner" -> {
                    array.add(R.drawable.difficulty_1)
                }
                "Intermediate" -> {
                    array.add(R.drawable.difficulty_2)
                }
                "Expert" -> {
                    array.add(R.drawable.difficulty_3)
                }
            }

            if (plantDetails.isPetFriendly == true) {
                array.add(R.drawable.safe_for_pets)
            }
            else {
                array.add(R.drawable.not_safe_for_pets)
            }

            when {
                plantDetails.cleansAir == true -> {
                    array.add(R.drawable.clean_air)
                }
                plantDetails.isColorful == true -> {
                    array.add(R.drawable.colorful)
                }
                plantDetails.isScented == true -> {
                    array.add(R.drawable.scented)
                }
            }
            gridview.numColumns = array.size
            return array
        }
    }
}