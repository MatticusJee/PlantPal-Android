package quay.plantpal.Activities

import android.content.Context
import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.facebook.*
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.snapchat.kit.sdk.SnapLogin
import com.snapchat.kit.sdk.core.controller.LoginStateController
import com.snapchat.kit.sdk.login.models.UserDataResponse
import com.snapchat.kit.sdk.login.networking.FetchUserDataCallback
import org.json.JSONObject
import quay.plantpal.BuildConfig.BUILD_TYPE
import quay.plantpal.Global
import quay.plantpal.Model.LoginType
import quay.plantpal.PlantPalAPI
import quay.plantpal.R
import java.security.MessageDigest
import java.util.*
import java.util.regex.Pattern


class CreateAccountActivity : AppCompatActivity() {

    private lateinit var email: EditText
    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var verifyPassword: EditText

    private lateinit var googleSignInClient: GoogleSignInClient
    private val googleActivityToken = 522

    private val callbackManager = CallbackManager.Factory.create()
    private val facebookActivityToken = 64206

    data class Result(val isValid: Boolean, val error: String? = null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)

        email = findViewById(R.id.create_email_textfield)
        username = findViewById(R.id.create_username_textfield)
        password = findViewById(R.id.create_password_textfield)
        verifyPassword = findViewById(R.id.create_verify_password_textfield)

        val plantpalSignUpButton = findViewById<Button>(R.id.plant_pal_signup)
        plantpalSignUpButton.setOnClickListener {
            val result = validateTextFields()
            if (result.isValid) {
                createAccount(username.text.toString(), email.text.toString(), password.text.toString(), LoginType.plantpal)
            }
            else {
                Global.displayErrorAlert(this, "Create Account Error", result.error)
            }
        }
        val matrix = ColorMatrix()
        matrix.setSaturation(0f)
        val filter = ColorMatrixColorFilter(matrix)

        @Suppress("ConstantConditionIf")
        if (BUILD_TYPE == "prod") SnapLogin.getAuthTokenManager(this).startTokenGrant()

        val snapchatSignUpButton = findViewById<Button>(R.id.snapchat_signup)
        snapchatSignUpButton.setOnClickListener {
            snapchatFetchLoginInformation()
        }

        val clientIdIsValid = googleValidateServerClientID()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build()
        val googleSignUpButton = findViewById<Button>(R.id.google_signup)
        googleSignUpButton.setOnClickListener {
            if (clientIdIsValid.isValid) {
                googleGetIdToken()
            }
            else {
                Global.displayErrorAlert(this, "Google Error", clientIdIsValid.error)
            }
        }
        if (clientIdIsValid.isValid) {
            googleSignUpButton.text = googleText()
            googleSignInClient = GoogleSignIn.getClient(this, gso)
        }
        else {
            googleSignUpButton.background.mutate().colorFilter = filter
        }

        val facebookPermissions = "email"
        val facebookSignUpButton = findViewById<LoginButton>(R.id.facebook_signup)
        facebookSignUpButton.setReadPermissions(listOf(facebookPermissions))
        facebookSignUpButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                val result = loginResult ?: return
                val accessToken = result.accessToken
                val request: GraphRequest = GraphRequest.newMeRequest(accessToken, object : GraphRequest.GraphJSONObjectCallback {
                    override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
                        val graphResponse = response ?: return
                        val email = graphResponse.jsonObject.getString("email")
                        val userId = graphResponse.jsonObject.getString("id")
                        val username = email.substringBefore("@")
                        val password = "${username}${userId}"
                        createAccount(username, email, password, LoginType.facebook)
                    }
                })
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() { }

            override fun onError(exception: FacebookException?) {
                Global.displayErrorAlert(this@CreateAccountActivity, "Facebook Error", "Handle SignIn Result Error")
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == googleActivityToken) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            googleHandleCreateAccountResult(task)
        }
        else if (requestCode == facebookActivityToken) {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun googleText(): SpannableString {
        val googleText = SpannableString("Google")
        val capitalGIndex = googleText.indexOf("G")
        val redOIndex = googleText.indexOf("o")
        val yellowOIndex = googleText.indexOf("o", 2)
        val lowercaseGIndex = googleText.indexOf("g")
        val greenLIndex = googleText.indexOf("l")
        val redEIndex = googleText.indexOf("e")
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_blue)), capitalGIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_red)), redOIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_yellow)), yellowOIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_blue)), lowercaseGIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_green)), greenLIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_red)), redEIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return googleText
    }

    private fun validateTextFields(): Result {
        val emailText = email.text.toString()
        val usernameText = username.text.toString()
        val passwordText = password.text.toString()
        val verifyPasswordText = verifyPassword.text.toString()
        if (emailText == "" || usernameText == "" || passwordText == "" || verifyPasswordText == "") {
            return Result(false, "One of the textfields is empty. Please make sure all fields are filled and try again.")
        }
        else if (!isEmailValid(emailText)) {
            return Result(false, "Please enter a valid email address and try again.")
        }
        else if (!isUsernameValid(usernameText) || usernameText.length < 5) {
            return Result(false, "Please make sure to enter a valid username that's at least 5 characters and doesn't have spaces")
        }
        else if (passwordText != verifyPasswordText) {
            return Result(false, "Your passwords do not match.")
        }
        else if (!isPasswordValid(passwordText)) {
            return Result(false, "Please make sure to enter a valid password. Your password has to contain at least one uppercase and lowercase letter, a number, and be at least 6 characters")
        }
        return Result(true)
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isUsernameValid(username: String): Boolean {
        val regexUsername = "([^ ]+)"
        val pattern = Pattern.compile(regexUsername)
        return pattern.matcher(username).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        val regexPassword = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,})"
        val pattern = Pattern.compile(regexPassword)
        return pattern.matcher(password).matches()
    }

    private fun createAccount(username: String, email: String, password: String, accountType: LoginType) {
        val hashedPassword = getHash(password)
        PlantPalAPI.createUser(username.toLowerCase(Locale.getDefault()), email.toLowerCase(Locale.getDefault()), hashedPassword, accountType) { success, user, error ->
            if (success) {
                val sharedPreferencesEditor = getSharedPreferences("UserData", Context.MODE_PRIVATE).edit()
                val userAsString = Gson().toJson(user)
                sharedPreferencesEditor.putString("user", userAsString)
                sharedPreferencesEditor.apply()
                Global.user = user!!
                Global.logEvent("Created Account")
                displaySurveyActivity()
            }
            else {
                Global.displayErrorAlert(this, "Plant Pal Error", error)
            }
        }
    }

    private fun getHash(password: String): String {
        val hexChars = "0123456789abcdef"
        val bytes = MessageDigest.getInstance("SHA-512").digest(password.toByteArray())
        val result = StringBuilder(bytes.size * 2)
        bytes.forEach {
            val i = it.toInt()
            result.append(hexChars[i shr 4 and 0x0f])
            result.append(hexChars[i and 0x0f])
        }
        return result.toString()
    }

    private fun displaySurveyActivity() {
        val intent = Intent(this, StartSurveyActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    // Snapchat

    private fun snapchatFetchLoginInformation() {
        val mLoginStateChangedListener = object : LoginStateController.OnLoginStateChangedListener {
            override fun onLoginSucceeded() {
                Global.logEvent("Snapchat onLoginSucceeded")
                getSnapchatDetails()
            }

            override fun onLoginFailed() {
                Global.logEvent("Snapchat onLoginFailed")
            }

            override fun onLogout() {
                Global.logEvent("Snapchat onLogout")
            }
        }
        SnapLogin.getLoginStateController(this).addOnLoginStateChangedListener(mLoginStateChangedListener)
    }

    private fun getSnapchatDetails() {
        val isUserLoggedIn = SnapLogin.isUserLoggedIn(this)
        if (isUserLoggedIn) {
            Global.logEvent("Snapchat User Logged In")
            val query = "{me{displayName, externalId}}"
            SnapLogin.fetchUserData(this, query, null, object : FetchUserDataCallback {
                override fun onSuccess(userDataResponse: UserDataResponse?) {
                    if (userDataResponse == null || userDataResponse.data == null) {
                        Global.logEvent("Snapchat User Data Response Empty")
                        return
                    }
                    val meData = userDataResponse.data.me ?: return
                    val username = meData.displayName.replace(" ", "").toLowerCase(Locale.getDefault())
                    val email = "$username@snapchat.com"
                    val externalId = meData.externalId
                    val password = "${username}${externalId}"
                    createAccount(username, email, password, LoginType.snapchat)
                }
                override fun onFailure(isNetworkError: Boolean, statusCode: Int) {
                    Global.logEvent("Snapchat OnFailure")
                }
            })
        }
    }

    // Google Login

    private fun googleValidateServerClientID() : Result {
        val serverClientId = getString(R.string.server_client_id)
        val suffix = ".apps.googleusercontent.com"
        if (!serverClientId.trim { it <= ' ' }.endsWith(suffix)) {
            val message = "Invalid server client ID in strings.xml, must end with $suffix"
            return Result(false, message)
        }
        return Result(true, null)
    }

    private fun googleGetIdToken() {
        // Show an account picker to let the user choose a Google account from the device.
        // If the GoogleSignInOptions only asks for IDToken and/or profile and/or email then no
        // consent screen will be shown here.
        val signInIntent: Intent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, googleActivityToken)
    }

//    private fun googleRefreshIdToken() {
//        googleSignInClient.silentSignIn().addOnCompleteListener(this) { task -> googleHandleCreateAccountResult(task) }
//    }

    private fun googleHandleCreateAccountResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            val idToken = account!!.idToken ?: return
            val userId = account.id ?: return
            val emailAddress = account.email ?: return
            val username = emailAddress.substringBefore("@")
            val password = "${username}${userId}"
            createAccount(username, emailAddress, password, LoginType.google)
        } catch (e: ApiException) {
            Global.displayErrorAlert(this, "Google Error", "Handle SignIn Result Error")
        }
    }

//    private fun googleSignOut() {
//        googleSignInClient.signOut().addOnCompleteListener(this) { }
//    }

//    private fun googleRevokeAccess() {
//        googleSignInClient.revokeAccess().addOnCompleteListener(this) { }
//    }
}