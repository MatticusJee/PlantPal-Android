package quay.plantpal.Activities

//import android.support.design.widget.Snackbar
//import android.support.v7.app.AppCompatActivity;

import android.content.Context
import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.facebook.*
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.snapchat.kit.sdk.SnapLogin
import com.snapchat.kit.sdk.core.controller.LoginStateController
import com.snapchat.kit.sdk.login.models.UserDataResponse
import com.snapchat.kit.sdk.login.networking.FetchUserDataCallback
import org.json.JSONObject
import quay.plantpal.Global
import quay.plantpal.Model.LoginType
import quay.plantpal.PlantPalAPI
import quay.plantpal.R
import java.security.MessageDigest
import java.util.*
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity() {

    private lateinit var usernameEmail: EditText
    private lateinit var password: EditText

    private lateinit var googleSignInClient: GoogleSignInClient
    private val googleActivityToken = 522

    private val callbackManager = CallbackManager.Factory.create()
    private val facebookActivityToken = 64206

    data class Result(val isValid: Boolean, val isEmail: Boolean, val error: String? = null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        usernameEmail = findViewById(R.id.username_email_textview)
        password = findViewById(R.id.plant_pal_signin_password)

        val plantpalLoginButton = findViewById<Button>(R.id.plant_pal_signin)
        plantpalLoginButton.setOnClickListener {
            val result = validateTextFields()
            if (result.isValid) {
                login(usernameEmail.text.toString(), password.text.toString(), result.isEmail, LoginType.plantpal)
            }
            else {
                Global.displayErrorAlert(this, "Login Error", result.error)
            }
        }
        val matrix = ColorMatrix()
        matrix.setSaturation(0f)
        val filter = ColorMatrixColorFilter(matrix)

        val snapchatLoginButton = findViewById<Button>(R.id.snapchat_login)
        snapchatLoginButton.setOnClickListener {
            SnapLogin.getAuthTokenManager(this).startTokenGrant()
            snapchatFetchLoginInformation()
        }

        val clientIdIsValid = validateServerClientID()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build()
        val googleLoginButton = findViewById<Button>(R.id.google_login)
        googleLoginButton.text = googleText()
        googleLoginButton.setOnClickListener {
            if (clientIdIsValid.isValid) {
                getIdToken()
            }
            else {
                Global.displayErrorAlert(this, "Google Error", clientIdIsValid.error)
            }
        }
        if (clientIdIsValid.isValid) {
            googleLoginButton.text = googleText()
            googleSignInClient = GoogleSignIn.getClient(this, gso)
        }
        else {
            googleLoginButton.background.mutate().colorFilter = filter
        }

        val facebookPermissions = "email"
        val facebookLoginButton = findViewById<LoginButton>(R.id.facebook_login)
        facebookLoginButton.setReadPermissions(listOf(facebookPermissions))
        facebookLoginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                val result = loginResult ?: return
                val accessToken = result.accessToken
                val request: GraphRequest = GraphRequest.newMeRequest(accessToken, object : GraphRequest.GraphJSONObjectCallback {
                    override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
                        val graphResponse = response ?: return
                        val email = graphResponse.jsonObject.getString("email")
                        val userId = graphResponse.jsonObject.getString("id")
                        val username = email.substringBefore("@")
                        val password = "${username}${userId}"
                        login(email, password, true, LoginType.facebook)
                    }
                })
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() { }

            override fun onError(exception: FacebookException?) {
                Global.displayErrorAlert(this@LoginActivity, "Facebook Error", "Handle SignIn Result Error")
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == googleActivityToken) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
        else if(requestCode == facebookActivityToken) {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun googleText(): SpannableString {
        val googleText = SpannableString("Google")
        val capitalGIndex = googleText.indexOf("G")
        val redOIndex = googleText.indexOf("o")
        val yellowOIndex = googleText.indexOf("o", 2)
        val lowercaseGIndex = googleText.indexOf("g")
        val greenLIndex = googleText.indexOf("l")
        val redEIndex = googleText.indexOf("e")
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_blue)), capitalGIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_red)), redOIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_yellow)), yellowOIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_blue)), lowercaseGIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_green)), greenLIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        googleText.setSpan(ForegroundColorSpan(ContextCompat.getColor(applicationContext, R.color.google_red)), redEIndex, googleText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return googleText
    }

    private fun validateTextFields(): Result {
        val emailUsernameText = usernameEmail.text.toString()
        val passwordText = password.text.toString()
        return if (emailUsernameText != "" && passwordText != "") {
            if (isEmailValid(emailUsernameText)) {
                Result(isValid = true, isEmail = true, error = null)
            }
            else if (isUsernameValid(emailUsernameText) && emailUsernameText.length >= 5) {
                Result(isValid = true, isEmail = false, error = null)
            }
            else {
                Result(isValid = false, isEmail = false, error = "Username/Email field is invalid")
            }
        }
        else {
            Result(isValid = false, isEmail = false, error = "One of the textfields is empty. Please make sure all fields are filled and try again.")
        }
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isUsernameValid(username: String): Boolean {
        val regexUsername = "([^ ]+)"
        val pattern = Pattern.compile(regexUsername)
        return pattern.matcher(username).matches()
    }

    private fun login(accountName: String, password: String, isEmail: Boolean, accountType: LoginType) {
        val hashedPassword = getHash(password)
        PlantPalAPI.login(accountName.toLowerCase(Locale.getDefault()), hashedPassword, isEmail, accountType) { success, user, hasUserPrefs, error ->
            if (success) {
                val sharedPreferencesEditor = getSharedPreferences("UserData", Context.MODE_PRIVATE).edit()
                val userAsString = Gson().toJson(user)
                sharedPreferencesEditor.putString("user", userAsString)
                sharedPreferencesEditor.putBoolean("userPrefIsSet", hasUserPrefs!!)
                sharedPreferencesEditor.apply()
                Global.user = user!!
                Global.prefsSet = hasUserPrefs
                if (hasUserPrefs == true) {
                    displayHomeActivity()
                    Global.logEvent("Login Successful", hashMapOf("Destination" to "Home"))
                }
                else {
                    displaySurveyActivity()
                    Global.logEvent("Login Successful", hashMapOf("Destination" to "Survey"))
                }
            }
            else {
                Global.displayErrorAlert(this, "Unable to Login", error)
            }
        }
    }

    private fun getHash(password: String): String {
        val hexChars = "0123456789abcdef"
        val bytes = MessageDigest.getInstance("SHA-512").digest(password.toByteArray())
        val result = StringBuilder(bytes.size * 2)
        bytes.forEach {
            val i = it.toInt()
            result.append(hexChars[i shr 4 and 0x0f])
            result.append(hexChars[i and 0x0f])
        }
        return result.toString()
    }

    private fun displaySurveyActivity() {
        val intent = Intent(this, StartSurveyActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun displayHomeActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    // Snapchat

    private fun snapchatFetchLoginInformation() {
        val mLoginStateChangedListener = object : LoginStateController.OnLoginStateChangedListener {
            override fun onLoginSucceeded() {
                Global.logEvent("Snapchat onLoginSucceeded")
                getSnapchatDetails()
            }

            override fun onLoginFailed() {
                Global.logEvent("Snapchat onLoginFailed")
            }

            override fun onLogout() {
                Global.logEvent("Snapchat onLogout")
            }
        }
        SnapLogin.getLoginStateController(this).addOnLoginStateChangedListener(mLoginStateChangedListener)
    }

    private fun getSnapchatDetails() {
        val isUserLoggedIn = SnapLogin.isUserLoggedIn(this)
        if (isUserLoggedIn) {
            Global.logEvent("Snapchat User Logged In")
            val query = "{me{displayName, externalId}}"
            SnapLogin.fetchUserData(this, query, null, object : FetchUserDataCallback {
                override fun onSuccess(userDataResponse: UserDataResponse?) {
                    if (userDataResponse == null || userDataResponse.data == null) {
                        Global.logEvent("Snapchat User Data Response Empty")
                        return
                    }
                    val meData = userDataResponse.data.me ?: return
                    val username = meData.displayName.replace(" ", "").toLowerCase(Locale.getDefault())
                    val externalId = meData.externalId
                    val password = "${username}${externalId}"
                    login(username, password, false, LoginType.snapchat)
                }
                override fun onFailure(isNetworkError: Boolean, statusCode: Int) {
                    Global.logEvent("Snapchat OnFailure")
                }
            })
        }
    }

    // Google

    private fun validateServerClientID() : CreateAccountActivity.Result {
        val serverClientId = getString(R.string.server_client_id)
        val suffix = ".apps.googleusercontent.com"
        if (!serverClientId.trim { it <= ' ' }.endsWith(suffix)) {
            val message = "Invalid server client ID in strings.xml, must end with $suffix"
            return CreateAccountActivity.Result(false, message)
        }
        return CreateAccountActivity.Result(true, null)
    }

    private fun getIdToken() {
        // Show an account picker to let the user choose a Google account from the device.
        // If the GoogleSignInOptions only asks for IDToken and/or profile and/or email then no
        // consent screen will be shown here.
        val signInIntent: Intent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, googleActivityToken)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            val idToken = account!!.idToken ?: return
            val userId = account.id ?: return
            val emailAddress = account.email ?: return
            val username = emailAddress.substringBefore("@")
            val password = "${username}${userId}"
            // TODO(developer): send ID Token to server and validate (Is what we should do, but we ain't doing that. We're just taking what Google gives us as is.)
            login(emailAddress, password, true, LoginType.google)
        } catch (e: ApiException) {
            Global.displayErrorAlert(this, "Google Error", "Handle SignIn Result Error")
        }
    }
}
